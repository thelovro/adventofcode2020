﻿using System;

namespace Days
{
    class Day01 : BaseDay
    {
        public Day01()
        {
            SampleInputPartOne.Add(@"1721
979
366
299
675
1456",
            "514579");

            SampleInputPartTwo.Add(@"1721
979
366
299
675
1456",
            "241861950");
        }

        protected override string FindFirstSolution(string[] input)
        {
            for (int i = 0; i < input.Length; i++)
            {
                for (int j = 0; j < input.Length; j++)
                {
                    int first = int.Parse(input[i]);
                    int second = int.Parse(input[j]);

                    if (first + second == 2020)
                        return (first * second).ToString();
                }
            }

            return "";
        }

        protected override string FindSecondSolution(string[] input)
        {
            for (int i = 0; i < input.Length; i++)
            {
                for (int j = 0; j < input.Length; j++)
                {
                    for (int k = 0; k < input.Length; k++)
                    {
                        int first = int.Parse(input[i]);
                        int second = int.Parse(input[j]);
                        int third = int.Parse(input[k]);

                        if (first + second + third == 2020)
                            return (first * second * third).ToString();
                    }
                }
            }

            return "";
        }
    }
}
