﻿using System.Linq;
using System;

namespace Days
{
    class Day02 : BaseDay
    {
        public Day02()
        {
            SampleInputPartOne.Add(@"1-3 a: abcde
1-3 b: cdefg
2-9 c: ccccccccc",
            "2");

            SampleInputPartTwo.Add(@"1-3 a: abcde
1-3 b: cdefg
2-9 c: ccccccccc",
            "1");
        }

        protected override string FindFirstSolution(string[] input)
        {
            int counter = 0;
            foreach (string line in input)
            {
                string[] parts = line.Split(' ');
                int minOccurance = int.Parse(parts[0].Split('-')[0]);
                int maxOccurance = int.Parse(parts[0].Split('-')[1]);

                char key = parts[1].TrimEnd(':')[0];

                int temp = parts[2].Where(x => x == key).Count();
                if (temp >= minOccurance && temp <= maxOccurance)
                {
                    counter++;
                }
            }

            return counter.ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            int counter = 0;
            foreach (string line in input)
            {
                string[] parts = line.Split(' ');
                int positionOne = int.Parse(parts[0].Split('-')[0]) - 1;
                int positionTwo = int.Parse(parts[0].Split('-')[1]) - 1;

                char key = parts[1].TrimEnd(':')[0];

                if (parts[2][positionOne] == key && parts[2][positionTwo] != key
                    ||
                    parts[2][positionOne] != key && parts[2][positionTwo] == key)
                {
                    counter++;
                }
            }

            return counter.ToString();
        }
    }
}
