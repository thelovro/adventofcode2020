﻿using System.Collections.Generic;

namespace Days
{
    class Day03 : BaseDay
    {
        public Day03()
        {
            SampleInputPartOne.Add(@"..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#", "7");

            SampleInputPartTwo.Add(@"..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#", "336");
        }

        protected override string FindFirstSolution(string[] input)
        {
            int numberOfTrees = 0;
            int col = 0;
            int rowLength = input[0].Length;

            for (int row = 0; row < input.Length; row++)
            {
                if (input[row][col % rowLength] == '#')
                {
                    numberOfTrees++;
                }

                col += 3;
            }

            return numberOfTrees.ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            List<(int, int)> slopes = new List<(int, int)>();
            slopes.Add((1, 1));
            slopes.Add((3, 1));
            slopes.Add((5, 1));
            slopes.Add((7, 1));
            slopes.Add((1, 2));

            int allTrees = 1;

            foreach (var slope in slopes)
            {
                int numberOfTrees = 0;
                int col = 0;
                int row = 0;
                int rowLength = input[0].Length;

                while(row < input.Length)
                {
                    if (input[row][col % rowLength] == '#')
                    {
                        numberOfTrees++;
                    }
                    row += slope.Item2;
                    col += slope.Item1;
                }

                allTrees *= numberOfTrees;
            }

            return allTrees.ToString();
        }
    }
}
