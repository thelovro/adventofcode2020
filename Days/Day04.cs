﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Days
{
    class Day04 : BaseDay
    {
        public Day04()
        {
            SampleInputPartOne.Add(@"ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in", "2");

            SampleInputPartTwo.Add(@"eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007

pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719
", "4");
        }

        protected override string FindFirstSolution(string[] input)
        {
            List<string> requiredFields = new List<string>() { "byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid" };

            int noValidPassports = 0;
            List<string> passportFields = new List<string>();

            foreach (var row in input)
            {
                if(row.Length == 0)
                {
                    //check whether any fields are missing
                    if(passportFields.Intersect(requiredFields).Count() == requiredFields.Count)
                    {
                        noValidPassports++;
                    }

                    //init new passport
                    passportFields = new List<string>();

                    continue;
                }

                foreach(string part in row.Split(' '))
                {
                    passportFields.Add(part.Split(':')[0]);
                }
            }

            return noValidPassports.ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            List<string> requiredFields = new List<string>() { "byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid" };

            int noValidPassports = 0;
            List<string> passportFields = new List<string>();

            foreach (var row in input)
            {
                if (row.Length == 0)
                {
                    //check whether any fields are missing
                    if (passportFields.Intersect(requiredFields).Count() == requiredFields.Count)
                    {
                        noValidPassports++;
                    }

                    //init new passport
                    passportFields = new List<string>();

                    continue;
                }

                foreach (string part in row.Split(' '))
                {
                    if (IsPartValid(part))
                    {
                        passportFields.Add(part.Split(':')[0]);
                    }
                }
            }

            return noValidPassports.ToString();
        }

        private static bool IsPartValid(string part)
        {
            string name = part.Split(':')[0];
            string value = part.Split(':')[1];

            bool isValid = name switch
            {
                "byr" => int.Parse(value) is >= 1920 and <= 2002,
                "iyr" => int.Parse(value) is >= 2010 and <= 2020,
                "eyr" => int.Parse(value) is >= 2020 and <= 2030,
                "hgt" => value.EndsWith("cm") ? int.Parse(value.Replace("cm", string.Empty)) is >= 150 and <= 193 :
                                            int.Parse(value.Replace("in", string.Empty)) is >= 59 and <= 76,
                "hcl" => value.Length == 7 && value[0] == '#',
                "ecl" => new List<string>() { "amb", "blu", "brn", "gry", "grn", "hzl", "oth" }.Contains(value),
                "pid" => value.Length == 9 && int.TryParse(value, out var _),
                "cid" => true,
                _ => throw new Exception("huh")
            };

            return isValid;
        }
    }
}
