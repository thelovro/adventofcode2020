﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Days
{
    class Day05 : BaseDay
    {
        public Day05()
        {
            SampleInputPartOne.Add(@"FBFBBFFRLR
BFFFBBFRRR
FFFBBBFRRR
BBFFBBFRLL", "820");
        }

        protected override string FindFirstSolution(string[] input)
        {
            List<Seat> seats = ParseSeats(input);

            return seats.Max(s => s.SeatId).ToString();
        }

        private List<Seat> ParseSeats(string[] input)
        {
            List<Seat> seats = new();

            foreach(string line in input)
            {
                int row = GetDecimal(line.Substring(0, 7));
                int col = GetDecimal(line.Substring(7, 3));

                seats.Add(new Seat(row, col, row * 8 + col));
            }

            return seats;
        }

        private int GetDecimal(string line)
        {
            var temp = line.Replace('F', '0').Replace('B', '1').Replace('L', '0').Replace('R', '1');

            return Convert.ToInt32(temp, 2);
        }

        public record Seat(int Row, int Column, int SeatId);

        protected override string FindSecondSolution(string[] input)
        {
            List<Seat> seats = ParseSeats(input);

            int result = Enumerable.Range(seats.Min(s => s.SeatId), seats.Max(s => s.SeatId))
                .Except(seats.Select(s => s.SeatId).ToList()).First();

            return result.ToString();
        }
    }
}
