﻿using System.Collections.Generic;
using System.Linq;

namespace Days
{
    class Day06 : BaseDay
    {
        public Day06()
        {
            SampleInputPartOne.Add(@"abc

a
b
c

ab
ac

a
a
a
a

b
", "11");

            SampleInputPartTwo.Add(@"abc

a
b
c

ab
ac

a
a
a
a

b
", "6");
        }

        protected override string FindFirstSolution(string[] input)
        {
            List<int> sum = new();

            List<char> questions = new();
            foreach(string line in input)
            {
                if(line.Trim().Length == 0)
                {
                    //end of group
                    sum.Add(questions.Count());
                    questions = new();

                    continue;
                }

                questions = questions.Union(line).ToList();
            }

            return sum.Sum().ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            List<int> sum = new();
            List<char> questions = Enumerable.Range('a', 26).Select(c => (char)c).ToList();

            foreach (string line in input)
            {
                if (line.Trim().Length == 0)
                {
                    sum.Add(questions.Count());

                    //reset questions list
                    questions = Enumerable.Range('a', 26).Select(c => (char)c).ToList();

                    continue;
                }
                
                questions = questions.Intersect(line).ToList();
            }

            return sum.Sum().ToString();
        }
    }
}
