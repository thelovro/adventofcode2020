﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Days
{
    class Day07 : BaseDay
    {
        public Day07()
        {
            SampleInputPartOne.Add(@"light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.", "4");

            SampleInputPartTwo.Add(@"shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags.", "126");
        }

        protected override string FindFirstSolution(string[] input)
        {
            List<Bag> bags = GetBags(input);

            CheckIfItCanContaingShinyGold(bags);

            return bags.Where(x => x.CanContainShinyGold).Count().ToString();
        }

        public class Bag
        {
            public string Color { get; set; }
            public List<(int, string)> Bags { get; set; }
            public bool CanContainShinyGold { get; set; }
        }

        private List<Bag> GetBags(string[] input)
        {
            List<Bag> bags = new();
            foreach (string line in input)
            {
                string tempLine = line.Replace("bags", "").Replace("bag", "").Replace(".", "");

                string[] parts = tempLine.Split(" contain ");
                List<(int, string)> containtedBags = new();

                if (parts[1] != "no other ")
                {
                    foreach (string bag in parts[1].Split(", "))
                    {
                        //define number of contained bags and concatenate a name :$
                        containtedBags.Add((Convert.ToInt32(bag.Split(" ")[0]),
                            $"{bag.Split(" ")[1].Trim()} {bag.Split(" ")[2].Trim()}"));
                    }
                }

                bags.Add(new Bag()
                {
                    Color = parts[0].Trim(),
                    Bags = containtedBags,
                    CanContainShinyGold = false
                });
            }

            return bags;
        }

        #region Part One
        private void CheckIfItCanContaingShinyGold(List<Bag> bags)
        {
            foreach(var bag in bags)
            {
                bag.CanContainShinyGold = CheckCanContainShinyGold(bags, bag);
            }
        }

        private bool CheckCanContainShinyGold(List<Bag> bags, Bag bag)
        {
            if (bag.Bags.Any(x => x.Item2 == "shiny gold"))
            {
                return true;
            }
            else if (bag.Bags.Count == 0)
            {
                return false;
            }
            else
            {
                bool canContain = false;
                foreach (var bagColor in bag.Bags)
                {
                    canContain = CheckCanContainShinyGold(bags, bags.First(b => b.Color == bagColor.Item2));
                    if (canContain) break;
                }

                return canContain;
            }
        }
        #endregion Part One
        
        protected override string FindSecondSolution(string[] input)
        {
            List<Bag> bags = GetBags(input);

            var shinyGoldBag = bags.First(b => b.Color == "shiny gold");
            int sum = CountBags(bags, shinyGoldBag);

            return sum.ToString();
        }
        #region Part Two

        private int CountBags(List<Bag> bags, Bag bag)
        {
            return bag.Bags.Sum(c => c.Item1 + c.Item1 * CountBags(bags, bags.First(b => b.Color == c.Item2)));
        }
        #endregion Part Two
    }
}
