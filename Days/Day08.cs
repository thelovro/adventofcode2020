﻿using System;
using System.Collections.Generic;
using System.Linq;
using static Days.Asm;

namespace Days
{
    class Day08 : BaseDay
    {
        public Day08()
        {
            SampleInputPartOne.Add(@"nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6", "5");

            SampleInputPartTwo.Add(@"nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6", "8");
        }

        protected override string FindFirstSolution(string[] input)
        {
            List<Operation> operations = ParseInput(input);

            int operationIndex = 0;
            int acumulatorValue = 0;

            while (true)
            {
                (bool isFinished, bool didEscapeLoop) = Asm.ParseOperation(ref operations, ref operationIndex, ref acumulatorValue);

                if (isFinished) break;
            }

            return acumulatorValue.ToString();
        }

        private List<Operation> ParseInput(string[] input)
        {
            List<Operation> operations = new();
            foreach(string line in input)
            {
                operations.Add(new Operation()
                {
                    Name = line.Split(' ')[0],
                    Step = Convert.ToInt32(line.Split(' ')[1]),
                    Visited = false
                });
            }

            return operations;
        }

        protected override string FindSecondSolution(string[] input)
        {
            List<Operation> operations = ParseInput(input);

            int acumulatorValue = 0;

            //we loop through all operations and interchange "jmp" and "nop" operations until one succeeds
            for (int i = 0; i < operations.Count; i++)
            {
                if (operations[i].Name == "acc")
                {
                    continue;
                }

                List<Operation> tmpOperations = (List<Operation>)operations.Clone<Operation>();
                if (tmpOperations[i].Name == "jmp")
                    tmpOperations[i].Name = "nop";
                else if (tmpOperations[i].Name == "nop")
                    tmpOperations[i].Name = "jmp";

                int operationIndex = 0;
                acumulatorValue = 0;

                while (true)
                {
                    (bool isFinished, bool didEscapeLoop) = ParseOperation(ref tmpOperations, ref operationIndex, ref acumulatorValue);

                    if (didEscapeLoop)
                    {
                        return acumulatorValue.ToString();
                    }

                    if (isFinished) break;
                }
            }

            throw new Exception("Should not happened!");
        }

        
    }

    public static class Asm
    {
        public static (bool, bool) ParseOperation(ref List<Operation> operations, ref int currentOperation, ref int acumulatorValue)
        {
            operations[currentOperation].Visited = true;

            if (operations[currentOperation].Name == "acc")
            {
                acumulatorValue += operations[currentOperation].Step;
            }

            currentOperation = operations[currentOperation].Name switch
            {
                "acc" => currentOperation + 1,
                "jmp" => currentOperation + operations[currentOperation].Step,
                "nop" => currentOperation + 1,
                _ => throw new Exception("Unknown operation!")
            };

            //did we escape the loop?
            if (currentOperation >= operations.Count)
            {
                return (true, true);
            }

            return (operations[currentOperation].Visited, false);
        }

        public class Operation : ICloneable
        {
            public string Name { get; set; }
            public int Step { get; set; }
            public bool Visited { get; set; }

            public object Clone()
            {
                return this.MemberwiseClone();
            }
        }

    }

    static class Extensions
    {
        public static IList<T> Clone<T>(this IList<T> listToClone) where T : ICloneable
        {
            return listToClone.Select(item => (T)item.Clone()).ToList();
        }
    }
}
