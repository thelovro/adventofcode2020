﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Days
{
    class Day09 : BaseDay
    {
        public Day09()
        {
            SampleInputPartOne.Add(@"35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576", "127");

            SampleInputPartTwo.Add(@"35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576", "62");
        }

        protected override string FindFirstSolution(string[] input)
        {
            long premable = IsTest ? 5 : 25;

            long problematicNumber = FindProblematicNumber(input, premable);

            return problematicNumber.ToString();
        }

        private static long FindProblematicNumber(string[] input, long premable)
        {
            long problematicNumber = 0;

            //find first number that does not add up from the last 25 numbers before it
            for (long i = premable; i < input.Length; i++)
            {
                long sum = Convert.ToInt64(input[i]);

                bool exists = false;
                for (long j = i - premable; j < i; j++)
                {
                    for (long k = i - premable; k < i; k++)
                    {
                        long num1 = Convert.ToInt64(input[j]);
                        long num2 = Convert.ToInt64(input[k]);
                        if (num1 != num2 && num1 + num2 == sum)
                            exists = true;
                    }
                }

                if (!exists)
                {
                    problematicNumber = sum;
                    break;
                }
            }

            return problematicNumber;
        }

        protected override string FindSecondSolution(string[] input)
        {
            long premable = IsTest ? 5 : 25;

            long problematicNumber = FindProblematicNumber(input, premable);
            
            List<long> items = new();
            long solutiuon = 0;

            //for the problematic number find all the contiguous numbers that add up that number
            for (int i = 0; i<input.Length;i++)
            {
                if(items.Sum() == problematicNumber)
                {
                    solutiuon = items.Min() + items.Max();
                    break;
                }

                while (items.Sum() > problematicNumber)
                {
                    items.RemoveAt(0);
                }

                if (items.Sum() < problematicNumber)
                {
                    items.Add(Convert.ToInt64(input[i]));
                }
            }

            return solutiuon.ToString();
        }
    }
}
