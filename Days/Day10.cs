﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Days
{
    class Day10 : BaseDay
    {
        public Day10()
        {
            SampleInputPartOne.Add(@"16
10
15
5
1
11
7
19
6
12
4", "35");

            SampleInputPartOne.Add(@"28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3", "220");

            SampleInputPartTwo.Add(@"16
10
15
5
1
11
7
19
6
12
4", "8");

            SampleInputPartTwo.Add(@"28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3", "19208");
        }

        protected override string FindFirstSolution(string[] input)
        {
            var list = input.Select(x => Convert.ToInt32(x)).OrderBy(x => x).ToList();

            int oneJoltDif = 1;
            int threeJoltDif = 0;
            for (int i=1;i<list.Count;i++)
            {
                if(list[i]-list[i-1] == 1)
                {
                    oneJoltDif++;
                }
                else if (list[i] - list[i - 1] == 3)
                {
                    threeJoltDif++;
                }
            }

            threeJoltDif++;

            return (oneJoltDif * threeJoltDif).ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            var list = input.Select(x => Convert.ToInt32(x)).OrderBy(x => x).ToList();

            Dictionary<int, long> items = new();
            items.Add(0, 1);

            long sum = 0;
            while (true)
            {
                Dictionary<int, long> _tmpItems = new();
                foreach (var item in items.GroupBy(x => new { x.Key, x.Value })
                                    .Select(x => new { itemVValue = x.Key.Key, multiplier = x.Key.Value, counter = x.Count() }))
                {
                    var candidates = list.Where(x => x > item.itemVValue && x <= item.itemVValue + 3).ToList();
                    if (candidates.Count() == 0)
                    {
                        //we reached the end of the list
                        sum += item.multiplier * item.counter;
                    }
                    else
                    {
                        //add new candidates to the list
                        for (int j = 0; j < candidates.Count(); j++)
                        {
                            if (_tmpItems.ContainsKey(candidates[j]))
                            {
                                _tmpItems[candidates[j]] += item.multiplier * item.counter;
                            }
                            else
                            {
                                _tmpItems.Add(candidates[j], item.multiplier * item.counter);
                            }
                        }
                    }
                }

                if (_tmpItems.Count == 0)
                    break;

                items = _tmpItems;
            }

            return sum.ToString();
        }
    }
}
