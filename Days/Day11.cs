﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Days
{
    class Day11 : BaseDay
    {
        public Day11()
        {
            SampleInputPartOne.Add(@"L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL", "37");

            SampleInputPartTwo.Add(@"L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL", "26");
        }

        const char Floor = '.';
        const char Empty = 'L';
        const char Occupied = '#';
        const char Unknown = ' ';

        protected override string FindFirstSolution(string[] input)
        {
            Dictionary<int, List<char>> seatArrangement = new();
            for (int i = 0; i < input.Length; i++)
            {
                seatArrangement.Add(i, input[i].ToList());
            }

            int tolerance = 4;

            while (true)
            {
                bool smthChanged = false;
                smthChanged = RunCycle(seatArrangement, tolerance, 1);

                if (!smthChanged)
                    break;
            }

            int sum = seatArrangement.Sum(row => row.Value.Where(s => s == Occupied).Count());

            return sum.ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            Dictionary<int, List<char>> seatArrangement = new();
            for (int i = 0; i < input.Length; i++)
            {
                seatArrangement.Add(i, input[i].ToList());
            }

            int tolerance = 5;

            while (true)
            {
                bool smthChanged = false;
                smthChanged = RunCycle(seatArrangement, tolerance);

                if (!smthChanged)
                    break;
            } 

            int sum = seatArrangement.Sum(row => row.Value.Where(s => s == Occupied).Count());

            return sum.ToString();
        }

        private static bool RunCycle(Dictionary<int, List<char>> seatArrangement, int tolerance, int limit = int.MaxValue)
        {
            List<(int, int)> occupy = new();
            List<(int, int)> leave = new();

            for (int row = 0; row < seatArrangement.Count; row++)
            {
                for (int col = 0; col < seatArrangement[0].Count; col++)
                {
                    if (seatArrangement[row][col] == Empty && ShouldYouSit(seatArrangement, row, col, limit))
                    {
                        occupy.Add((row, col));
                    }
                    if (seatArrangement[row][col] == Occupied && ShouldYouLeave(seatArrangement, row, col, tolerance, limit))
                    {
                        leave.Add((row, col));
                    }
                }
            }

            foreach (var item in occupy)
            {
                seatArrangement[item.Item1][item.Item2] = Occupied;
            }
            foreach (var item in leave)
            {
                seatArrangement[item.Item1][item.Item2] = Empty;
            }

            return occupy.Count > 0 || leave.Count > 0;
        }
        
        private static bool ShouldYouSit(Dictionary<int, List<char>> seatArrangement, int row, int col, int limit)
        {
            return NumberOfOccupiedSeats(seatArrangement, row, col, limit) == 0;
        }

        private static bool ShouldYouLeave(Dictionary<int, List<char>> seatArrangement, int row, int col, int tolerance, int limit)
        {
            return NumberOfOccupiedSeats(seatArrangement, row, col, limit) >= tolerance;
        }

        private static int NumberOfOccupiedSeats (Dictionary<int, List<char>> seatArrangement, int row, int col, int limit)
        {
            int sum = 0;
            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    if (i == 0 && j == 0) continue;

                    if (GetPositionStatus(seatArrangement, row, col, i, j, limit) == Occupied) sum++;
                }
            }

            return sum;
        }

        private static char GetPositionStatus(Dictionary<int, List<char>> seatArrangement, int row, int col, int directionRow, int directionCol, int limit)
        {
            int maxLength = seatArrangement.Count;
            int maxWidth = seatArrangement[0].Count;
            char seat = Unknown;
            int counter = 1;

            while (seat == Unknown)
            {
                int tmpDirectionRow = row + (directionRow * counter);
                int tmpDirectionCol = col + (directionCol * counter);
                counter++;

                if ((tmpDirectionRow) < 0 ||
                    (tmpDirectionCol) < 0 ||
                    (tmpDirectionRow) >= maxLength ||
                    (tmpDirectionCol) >= maxWidth)
                {
                    seat = Empty;
                    continue;
                }

                if (seatArrangement[tmpDirectionRow][tmpDirectionCol] != Floor)
                {
                    seat = seatArrangement[tmpDirectionRow][tmpDirectionCol];
                }

                if (counter > limit) break;
            }

            return seat;
        }
    }
}
