﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Days
{
    class Day12 : BaseDay
    {
        public Day12()
        {
            SampleInputPartOne.Add(@"F10
N3
F7
R90
F11", "25");

            SampleInputPartTwo.Add(@"F10
N3
F7
R90
F11", "286");
        }

        enum Direction
        {
            North,
            East,
            South,
            West
        }

        protected override string FindFirstSolution(string[] input)
        {
            Direction currentDirection = Direction.East;
            (int, int) position = (0, 0);

            foreach(string line in input)
            {
                int num = Convert.ToInt32(line[1..]);
                
                position.Item1 = line[0] switch
                {
                    'W' => position.Item1 - num,
                    'E' => position.Item1 + num,
                    'F' => currentDirection == Direction.East ? position.Item1 + num :
                        currentDirection == Direction.West ? position.Item1 - num :
                        position.Item1,
                    _ => position.Item1
                };

                position.Item2 = line[0] switch
                {
                    'N' => position.Item2 + num,
                    'S' => position.Item2 - num,
                    'F' => currentDirection == Direction.North ? position.Item2 + num :
                        currentDirection == Direction.South ? position.Item2 - num :
                        position.Item2,
                    _ => position.Item2
                };

                if(line[0] == 'R' || line[0] == 'L')
                {
                    currentDirection = Rotate(currentDirection, line[0] == 'R' ? 1 : -1, num / 90);
                }
            }

            return (Math.Abs(position.Item1) + Math.Abs(position.Item2)).ToString();
        }

        private Direction Rotate(Direction currentDirection, int direction, int factor)
        {
            for (int i = 0;i< factor; i++)
            {
                currentDirection += direction;
                if ((int)currentDirection == 4)
                    currentDirection = (Direction)0;
                if ((int)currentDirection == -1)
                    currentDirection = (Direction)3;
            }

            return currentDirection;
        }

        protected override string FindSecondSolution(string[] input)
        {
            (int, int) shipPosition = (0, 0);
            (int, int) waypointPosition = (10, 1);

            foreach (string line in input)
            {
                int num = Convert.ToInt32(line[1..]);

                if(line[0] == 'F')
                {
                    shipPosition = MoveShip(line, shipPosition, waypointPosition);
                }
                else
                {
                    waypointPosition = MoveWaypoint(line, waypointPosition);
                }
            }

            return (Math.Abs(shipPosition.Item1) + Math.Abs(shipPosition.Item2)).ToString();
        }

        private (int, int) MoveWaypoint(string line, (int, int) position)
        {
            int num = Convert.ToInt32(line[1..]);

            position.Item1 = line[0] switch
            {
                'E' => position.Item1 + num,
                'W' => position.Item1 - num,
                _ => position.Item1
            };

            position.Item2 = line[0] switch
            {
                'N' => position.Item2 + num,
                'S' => position.Item2 - num,
                _ => position.Item2
            };

            if(line[0] == 'R' || line[0] == 'L')
            {
                position = RotateWaypoint(position, num / 90, line[0] == 'R' ? 1 : -1);
            }

            return position;
        }

        private (int, int) RotateWaypoint((int, int) position, int factor, int direction)
        {
            (int, int, int, int) currentPosition = (
                position.Item2 > 0 ? position.Item2 : 0,
                position.Item1 > 0 ? position.Item1 : 0,
                position.Item2 < 0 ? Math.Abs(position.Item2) : 0,
                position.Item1 < 0 ? Math.Abs(position.Item1) : 0);

            (int, int, int, int) newPosition = (0, 0, 0, 0);
            if (direction == 1)
            {
                for (int i = 0; i < factor; i++)
                {
                    newPosition.Item1 = currentPosition.Item4;
                    newPosition.Item2 = currentPosition.Item1;
                    newPosition.Item3 = currentPosition.Item2;
                    newPosition.Item4 = currentPosition.Item3;

                    currentPosition = newPosition;
                }
            }
            else
            {
                for (int i = 0; i < factor; i++)
                {
                    newPosition.Item1 = currentPosition.Item2;
                    newPosition.Item2 = currentPosition.Item3;
                    newPosition.Item3 = currentPosition.Item4;
                    newPosition.Item4 = currentPosition.Item1;

                    currentPosition = newPosition;
                }
            }

            return (newPosition.Item2 - newPosition.Item4, newPosition.Item1 - newPosition.Item3);
        }

        private (int, int) MoveShip(string line, (int, int) shipPosition, (int, int) waypointPosition)
        {
            int factor = Convert.ToInt32(line[1..]);

            shipPosition.Item1 += waypointPosition.Item1 * factor;
            shipPosition.Item2 += waypointPosition.Item2 * factor;

            return shipPosition;
        }
    }
}
