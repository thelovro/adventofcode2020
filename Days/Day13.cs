﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace Days
{
    class Day13 : BaseDay
    {
        public Day13()
        {
            SampleInputPartOne.Add(@"939
7,13,x,x,59,x,31,19", "295");

            SampleInputPartTwo.Add(@"
7,13,x,x,59,x,31,19", "1068781");
            SampleInputPartTwo.Add(@"
17,x,13,19", "3417");
            SampleInputPartTwo.Add(@"
67,7,59,61", "754018");
            SampleInputPartTwo.Add(@"
67,x,7,59,61", "779210");
            SampleInputPartTwo.Add(@"
67,7,x,59,61", "1261476");
            SampleInputPartTwo.Add(@"
1789,37,47,1889", "1202161486");
        }

        protected override string FindFirstSolution(string[] input)
        {
            int limit = Convert.ToInt32(input[0]);
            List<int> busses = input[1].Split(',').Where(i => i != "x").Select(i => Convert.ToInt32(i)).ToList();

            Dictionary<int, int> results = new();
            foreach (var bus in busses)
            {
                int factor = limit / bus;
                if (factor * bus < limit)
                {
                    factor++;
                }

                results.Add(bus, factor * bus);
            }

            var final = results.First(b => b.Value == results.Min(x => x.Value));

            return (final.Key * (final.Value-limit)).ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            Dictionary<int, int> busses = ParseBusses(input[1]);

            BigInteger solution = FindLongLastingSolution(busses);

            return solution.ToString();
        }

        private static Dictionary<int, int> ParseBusses(string input)
        {
            Dictionary<int, int> busses = new();

            var tmpInput = input.Split(',');

            for (int i = 0; i < tmpInput.Length; i++)
            {
                if ((tmpInput[i]) == "x") continue;

                busses.Add(Convert.ToInt32(tmpInput[i]), i);
            }

            return busses;
        }

        /// <summary>
        /// first we take 2 largest busses, to maximize the step size!
        /// using example 1:
        ///   59*x = (T+4) + W
        ///   31*y = (T+6) + W
        ///   
        /// We have to calculate X and W
        /// </summary>
        /// <param name="busses"></param>
        /// <returns>number of minutes</returns>
        private BigInteger FindLongLastingSolution(Dictionary<int, int> busses)
        {
            //take two busses with larger ID
            var twoBusesWithLargestId = busses.OrderByDescending(b => b.Key).ToDictionary(x => x.Key, x => x.Value).Take(2).ToDictionary(x => x.Key, x => x.Value);

            var busWithMaxId = busses.First(b => b.Key == busses.Max(m => m.Key));
            //find solution only for two busses
            //as soluton we get a magic number for two busses
            BigInteger twoBussesSolution = DoTheMagic_Slow(twoBusesWithLargestId, busWithMaxId);

            //calculate X (see example in summary)
            BigInteger factor = (twoBussesSolution + busWithMaxId.Value) / busWithMaxId.Key;

            //get step size (=W) for biggest bus
            BigInteger stepSize = 1;
            foreach(var bus in twoBusesWithLargestId)
            {
                stepSize *= bus.Key;
            }

            BigInteger solution = DoTheMagic_Fast(busses, busWithMaxId, factor, stepSize);

            return solution;
        }

        private static BigInteger DoTheMagic_Slow(Dictionary<int, int> busses, KeyValuePair<int, int> busWithMaxId)
        {
            //find ghte biggest bus (because we calculated the factor for it!)
            BigInteger busMaxId = busWithMaxId.Key;
            BigInteger busMaxMinutes = busWithMaxId.Value;
            BigInteger factor = 1;
            BigInteger solution;

            while (true)
            {
                BigInteger numberToCheck = busMaxId * factor++ - busMaxMinutes;

                if (IsNumberSuitableForAllBusses(busses, numberToCheck))
                {
                    solution = numberToCheck;
                    break;
                }
            }

            return solution;
        }


        private BigInteger DoTheMagic_Fast(Dictionary<int, int> busses, KeyValuePair<int, int> busWithMaxId, BigInteger baseFactor, BigInteger stepSize)
        {
            BigInteger busMaxId = busWithMaxId.Key;
            BigInteger busMaxMinutes = busWithMaxId.Value;

            BigInteger solution;
            BigInteger counter = 0;

            if (!IsTest)
            {
                //a little bit of cheating :) Had to wait for 15 minutes of so, to calculate from 100000000000000 as author suggested
                counter = 725000000000000 / stepSize;
            }

            while (true)
            {
                BigInteger w = stepSize * counter++;
                BigInteger numberToCheck = w + baseFactor * busMaxId - busMaxMinutes;

                if (IsNumberSuitableForAllBusses(busses, numberToCheck))
                {
                    solution = numberToCheck;
                    break;
                }
            }

            return solution;
        }

        private static bool IsNumberSuitableForAllBusses(Dictionary<int, int> busses, BigInteger numberToCheck)
        {
            bool possibleCandidate = true;

            foreach (var bus in busses)
            {
                if ((numberToCheck + bus.Value) % bus.Key > 0)
                    possibleCandidate = false;

                if (!possibleCandidate)
                    break;
            }

            return possibleCandidate;
        }
    }
}
