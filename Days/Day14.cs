﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Days
{
    class Day14 : BaseDay
    {
        public Day14()
        {
            SampleInputPartOne.Add(@"mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
mem[8] = 11
mem[7] = 101
mem[8] = 0", "165");

            SampleInputPartTwo.Add(@"mask = 000000000000000000000000000000X1001X
mem[42] = 100
mask = 00000000000000000000000000000000X0XX
mem[26] = 1", "208");
        }

        protected override string FindFirstSolution(string[] input)
        {
            Dictionary<int, long> mem = new();

            string mask = "";
            foreach (string line in input)
            {
                if(line.StartsWith("mask"))
                {
                    mask = line.Replace("mask = ", "");
                    continue;
                }

                var parts = line.Split(" = ");
                int address = Convert.ToInt32(parts[0].Replace("mem[", "").Replace("]", ""));
                int value = Convert.ToInt32(parts[1]);

                List<char> binary = Convert.ToString(value, 2).PadLeft(36,'0').ToList();

                for(int b =0;b<mask.Length;b++)
                {
                    if (mask[b] == 'X') continue;

                    binary[b] = mask[b];
                }

                var newValueArray = string.Join("", binary.ToArray());
                long newValue = Convert.ToInt64(newValueArray, 2);
                if (mem.ContainsKey(address))
                {
                    mem[address] = newValue;
                }
                else
                {
                    mem.Add(address, newValue);
                }
            }

            return mem.Sum(m=>m.Value).ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            Dictionary<long, long> mem = new();

            string mask = "";
            foreach (string line in input)
            {
                if (line.StartsWith("mask"))
                {
                    mask = line.Replace("mask = ", "");
                    continue;
                }

                var parts = line.Split(" = ");
                long address = Convert.ToInt64(parts[0].Replace("mem[", "").Replace("]", ""));
                long value = Convert.ToInt64(parts[1]);

                List<char> binary = Convert.ToString(address, 2).PadLeft(36, '0').ToList();

                for (int b = 0; b < mask.Length; b++)
                {
                    if (mask[b] == '0') continue;

                    binary[b] = mask[b];
                }

                foreach (long newAddress in GetAddresses(binary))
                {
                    if (mem.ContainsKey(newAddress))
                    {
                        mem[newAddress] = value;
                    }
                    else
                    {
                        mem.Add(newAddress, value);
                    }
                }
            }

            return mem.Sum(m => m.Value).ToString();
        }

        private IEnumerable<long> GetAddresses(List<char> binary)
        {
            List<long> addresses = new();

            int sum = binary.Count(n => n == 'X');
            for (long i = 0; i < Math.Pow(2, sum); i++)
            {
                List<char> newBinary = Convert.ToString(0, 2).PadLeft(36, '0').ToList();
                for (int x = 0; x < binary.Count; x++)
                {
                    newBinary[x] = binary[x];
                }

                List<char> newBinaryLoop = Convert.ToString(i, 2).PadLeft(sum, '0').ToList();
                foreach (char z in newBinaryLoop)
                {
                    newBinary[newBinary.IndexOf('X')] = z;
                }

                var newAddressArray = string.Join("", newBinary.ToArray());
                addresses.Add(Convert.ToInt64(newAddressArray, 2));
            }

            return addresses;
        }
    }
}
