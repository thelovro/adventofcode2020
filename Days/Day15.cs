﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Days
{
    class Day15 : BaseDay
    {
        public Day15()
        {
            SampleInputPartOne.Add(@"0,3,6", "436");
            SampleInputPartOne.Add(@"1,3,2", "1");
            SampleInputPartOne.Add(@"2,1,3", "10");
            SampleInputPartOne.Add(@"1,2,3", "27");
            SampleInputPartOne.Add(@"2,3,1", "78");
            SampleInputPartOne.Add(@"3,2,1", "438");
            SampleInputPartOne.Add(@"3,1,2", "1836");

            SampleInputPartTwo.Add(@"0,3,6", "175594");
            SampleInputPartTwo.Add(@"1,3,2", "2578");
            SampleInputPartTwo.Add(@"2,1,3", "3544142");
            SampleInputPartTwo.Add(@"1,2,3", "261214");
            SampleInputPartTwo.Add(@"2,3,1", "6895259");
            SampleInputPartTwo.Add(@"3,2,1", "18");
            SampleInputPartTwo.Add(@"3,1,2", "362");
        }

        protected override string FindFirstSolution(string[] input)
        {
            long result = PlayTheGame(input, 2020);

            return result.ToString();
        }

        private long PlayTheGame(string[] input, long targetStep)
        {
            Dictionary<long, List<long>> log = new();
            long step = 1;
            long lastNumber = 0;
            foreach (var num in input[0].Split(','))
            {
                lastNumber = Convert.ToInt64(num);
                log.Add(lastNumber, new List<long>() { step++ });
            }

            long newNumber = 0;
            while (step <= targetStep)
            {
                newNumber = GetNewNumber(log[lastNumber]);
                lastNumber = newNumber;
                if (log.ContainsKey(newNumber))
                {
                    log[newNumber].Add(step);
                }
                else
                {
                    log.Add(newNumber, new List<long>() { step });
                }

                step++;
            }

            return log.First(l => l.Value.Contains(targetStep)).Key;
        }

        private long GetNewNumber(List<long> list)
        {
            if(list.Count == 1)
            {
                return 0;
            }

            return list[list.Count - 1] - list[list.Count - 2];
        }

        protected override string FindSecondSolution(string[] input)
        {
            long result = PlayTheGame(input, 30000000);

            return result.ToString();
        }
    }
}
