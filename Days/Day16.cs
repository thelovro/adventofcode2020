﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Days
{
    class Day16 : BaseDay
    {
        public Day16()
        {
            SampleInputPartOne.Add(@"class: 1-3 or 5-7
row: 6-11 or 33-44
seat: 13-40 or 45-50

your ticket:
7,1,14

nearby tickets:
7,3,47
40,4,50
55,2,20
38,6,12", "71");

            SampleInputPartTwo.Add(@"class: 0-1 or 4-19
row: 0-5 or 8-19
seat: 0-13 or 16-19

your ticket:
11,12,13

nearby tickets:
3,9,18
15,1,5
5,14,9", "13");
        }

        protected override string FindFirstSolution(string[] input)
        {
            Dictionary<string, List<int>> rules = ParseRules(input);
            Dictionary<int, List<int>> tickets = ParseTickets(input);

            long result = CheckTickets(rules, tickets);

            return result.ToString();
        }

        private long CheckTickets(Dictionary<string, List<int>> rules, Dictionary<int, List<int>> tickets)
        {
            long sum = 0;
            foreach(var ticket in tickets)
            {
                foreach(var item in ticket.Value)
                {
                    if(!rules.Any(r=>r.Value.Contains(item)))
                    {
                        sum += item;
                    }
                }
            }

            return sum;
        }

        private Dictionary<string, List<int>> ParseRules(string[] input)
        {
            Dictionary<string, List<int>> rules = new();

            foreach (var line in input)
            {
                if (line.Trim().Length == 0) break;

                string ruleName = line.Split(':')[0];
                List<int> items = new();

                string[] ranges = line.Split(':')[1].TrimStart().TrimEnd().Split(" or ");
                foreach(string range in ranges)
                {
                    var nums = range.Split('-');
                    for (int i = Convert.ToInt32(nums[0]); i <= Convert.ToInt32(nums[1]); i++)
                    {
                        items.Add(i);
                    }
                }

                rules.Add(ruleName, items);
            }

            return rules;
        }

        private Dictionary<int, List<int>> ParseTickets(string[] input)
        {
            Dictionary<int, List<int>> tickets = new();

            int index = input.ToList().IndexOf("your ticket:");
            tickets.Add(0, input[index + 1].Split(',').Select(n => Convert.ToInt32(n)).ToList());

            index = input.ToList().IndexOf("nearby tickets:") + 1;

            int counter = 1;
            for(int i=index;i<input.Length;i++)
            {
                tickets.Add(counter++, input[i].Split(',').Select(n => Convert.ToInt32(n)).ToList());
            }

            return tickets;
        }


        protected override string FindSecondSolution(string[] input)
        {
            Dictionary<string, List<int>> rules = ParseRules(input);
            Dictionary<int, List<int>> tickets = ParseTickets(input);

            RemoveInvalidTickets(rules, ref tickets);

            Dictionary<string, int> rulesPositions = FindRulesPosition(rules, tickets);

            long result = GetResult(rulesPositions, tickets[0]);

            return result.ToString();
        }

        private long GetResult(Dictionary<string, int> rulesPositions, List<int> lists)
        {
            string filter = IsTest ? "seat" : "departure";
            long result = 1;

            foreach(var rule in rulesPositions.Where(r=>r.Key.StartsWith(filter)))
            {
                result *= lists[rule.Value];
            }

            return result;
        }

        private Dictionary<string, int> FindRulesPosition(Dictionary<string, List<int>> rules, Dictionary<int, List<int>> tickets)
        {
            Dictionary<string, int> rulesPositions = new();

            while (rulesPositions.Count < rules.Count)
            {
                foreach (var rule in rules)
                {
                    if (rulesPositions.ContainsKey(rule.Key)) continue;

                    int okRules = 0;
                    int indexOkRule = -1;

                    for(int i=0;i<rules.Count;i++)
                    {
                        if (rulesPositions.Any(r => r.Value == i)) continue;

                        bool ruleValidForColumn = true;

                        foreach (var ticket in tickets)
                        {

                            if (!rule.Value.Contains(ticket.Value[i]))
                            {
                                ruleValidForColumn = false;
                                break;
                            }
                        }

                        if (ruleValidForColumn)
                        {
                            okRules++;
                            indexOkRule = i;
                        }
                    }

                    if(okRules == 1)
                    {
                        rulesPositions.Add(rule.Key, indexOkRule);
                    }
                }
            }

            return rulesPositions;
        }

        private void RemoveInvalidTickets(Dictionary<string, List<int>> rules, ref Dictionary<int, List<int>> tickets)
        {
            List<int> ticketsToRemove = new();
            for(int i=tickets.Count-1;i>=0;i--)
            {
                foreach (var item in tickets[i])
                {
                    if (!rules.Any(r => r.Value.Contains(item)))
                    {
                        tickets.Remove(i);
                    }
                }
            }
        }
    }
}
