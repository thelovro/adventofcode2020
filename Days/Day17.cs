﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Days
{
    class Day17 : BaseDay
    {
        public Day17()
        {
            SampleInputPartOne.Add(@".#.
..#
###", "112");

            SampleInputPartTwo.Add(@".#.
..#
###", "848");
        }

        const char Active = '#';
        const char Inactive = '.';

        protected override string FindFirstSolution(string[] input)
        {
            Dictionary<(int, int, int), char> cubes = GetInitialCubes(input);

            for (int i = 0; i < 6; i++)
            {
                cubes = RunCycle(cubes);
            }

            return cubes.Where(c=>c.Value== Active).Count().ToString();
        }

        private Dictionary<(int, int, int), char> GetInitialCubes(string[] input)
        {
            Dictionary<(int, int, int), char> cubes = new();

            int nuberOfAdditionalLayers = 6;
            for (int z = 0-nuberOfAdditionalLayers; z < (nuberOfAdditionalLayers+ 1); z++)
            {
                for (int y = 0 - nuberOfAdditionalLayers; y < input.Length + nuberOfAdditionalLayers; y++)
                {
                    for (int x = 0 - nuberOfAdditionalLayers; x < input.Length + nuberOfAdditionalLayers; x++)
                    {
                        char value = z == 0 && y >= 0 && y < input.Length && x >= 0 && x < input.Length ? input[y][x] : Inactive;
                        cubes.Add((x, y, z), value);
                    }
                }
            }

            return cubes;
        }

        private static Dictionary<(int, int, int), char> RunCycle(Dictionary<(int, int, int), char> cubes)
        {
            Dictionary<(int, int, int), char> newCubes = new();
            foreach (var cube in cubes)
            {
                newCubes.Add((cube.Key.Item1, cube.Key.Item2, cube.Key.Item3), DetermineNewStatus(cubes, cube.Key.Item1, cube.Key.Item2, cube.Key.Item3));
            }

            return newCubes;
        }

        private static char DetermineNewStatus(Dictionary<(int, int, int), char> cubes, int x, int y, int z)
        {
            int sum = GetPositionStatus(cubes, x,y,z);

            if (cubes[(x, y, z)] == Inactive && sum == 3
                ||
                cubes[(x, y, z)] == Active && sum == 2
                ||
                cubes[(x, y, z)] == Active && sum == 3)
                return Active;

            return Inactive;
        }

        private static int GetPositionStatus(Dictionary<(int, int, int), char> cubes, int initx, int inity, int initz)
        {
            int sum = 0;
            for (int x = initx - 1; x <= initx + 1; x++)
            {
                for (int y = inity - 1; y <= inity + 1; y++)
                {
                    for (int z = initz - 1; z <= initz + 1; z++)
                    {
                        if (x == initx && y == inity && z == initz) continue;

                        if(cubes.ContainsKey((x,y,z)) && cubes[(x,y,z)] == Active)
                        {
                            sum++;
                        }
                    }
                }
            }

            return sum;
        }

        protected override string FindSecondSolution(string[] input)
        {
            Dictionary<(int, int, int, int), char> cubes = GetInitialCubesPartTwo(input);

            for (int i = 0; i < 6; i++)
            {
                cubes = RunCyclePartTwo(cubes);
            }

            return cubes.Where(c => c.Value == Active).Count().ToString();
        }

        private Dictionary<(int, int, int, int), char> GetInitialCubesPartTwo(string[] input)
        {
            Dictionary<(int, int, int, int), char> cubes = new();

            int nuberOfAdditionalLayers = 6;
            for (int w = 0 - nuberOfAdditionalLayers; w < (nuberOfAdditionalLayers + 1); w++)
            {
                for (int z = 0 - nuberOfAdditionalLayers; z < (nuberOfAdditionalLayers + 1); z++)
                {
                    for (int y = 0 - nuberOfAdditionalLayers; y < input.Length + nuberOfAdditionalLayers; y++)
                    {
                        for (int x = 0 - nuberOfAdditionalLayers; x < input.Length + nuberOfAdditionalLayers; x++)
                        {
                            char value = w == 0 && z == 0 && y >= 0 && y < input.Length && x >= 0 && x < input.Length ? input[y][x] : Inactive;
                            cubes.Add((x, y, z, w), value);
                        }
                    }
                }
            }

            return cubes;
        }

        private static Dictionary<(int, int, int, int), char> RunCyclePartTwo(Dictionary<(int, int, int, int), char> cubes)
        {
            Dictionary<(int, int, int, int), char> newCubes = new();
            foreach (var cube in cubes)
            {
                newCubes.Add((cube.Key.Item1, cube.Key.Item2, cube.Key.Item3, cube.Key.Item4), DetermineNewStatusPartTwo(cubes, cube.Key.Item1, cube.Key.Item2, cube.Key.Item3, cube.Key.Item4));
            }

            return newCubes;
        }

        private static char DetermineNewStatusPartTwo(Dictionary<(int, int, int, int), char> cubes, int x, int y, int z, int w)
        {
            int sum = GetPositionStatusPartTwo(cubes, x, y, z, w);

            if (cubes[(x, y, z, w)] == Inactive && sum == 3
                ||
                cubes[(x, y, z, w)] == Active && sum == 2
                ||
                cubes[(x, y, z, w)] == Active && sum == 3)
                return Active;

            return Inactive;
        }

        private static int GetPositionStatusPartTwo(Dictionary<(int, int, int, int), char> cubes, int initx, int inity, int initz, int initw)
        {
            int sum = 0;
            for (int x = initx - 1; x <= initx + 1; x++)
            {
                for (int y = inity - 1; y <= inity + 1; y++)
                {
                    for (int z = initz - 1; z <= initz + 1; z++)
                    {
                        for (int w = initw - 1; w <= initw + 1; w++)
                        {
                            if (x == initx && y == inity && z == initz && w == initw) continue;

                            if (cubes.ContainsKey((x, y, z, w)) && cubes[(x, y, z, w)] == Active)
                            {
                                sum++;
                            }
                        }
                    }
                }
            }

            return sum;
        }
    }
}
