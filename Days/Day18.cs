﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Days
{
    class Day18 : BaseDay
    {
        public Day18()
        {
            SampleInputPartOne.Add(@"1 + 2 * 3 + 4 * 5 + 6", "71");
            SampleInputPartOne.Add(@"1 + (2 * 3) + (4 * (5 + 6))", "51");
            SampleInputPartOne.Add(@"2 * 3 + (4 * 5)", "26");
            SampleInputPartOne.Add(@"5 + (8 * 3 + 9 + 3 * 4 * 3)", "437");
            SampleInputPartOne.Add(@"5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))", "12240");
            SampleInputPartOne.Add(@"((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", "13632");

            SampleInputPartTwo.Add(@"1 + 2 * 3 + 4 * 5 + 6", "231");
            SampleInputPartTwo.Add(@"1 + (2 * 3) + (4 * (5 + 6))", "51");
            SampleInputPartTwo.Add(@"2 * 3 + (4 * 5)", "46");
            SampleInputPartTwo.Add(@"5 + (8 * 3 + 9 + 3 * 4 * 3)", "1445");
            SampleInputPartTwo.Add(@"5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))", "669060");
            SampleInputPartTwo.Add(@"((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", "23340");
        }

        protected override string FindFirstSolution(string[] input)
        {
            long sum = 0;
            foreach(string line in input)
            {
                string modifiedLine = line.Replace(" ", "");
                sum += CalcualteSum(modifiedLine);
            }

            return sum.ToString();
        }

        private long CalcualteSum(string line)
        {
            line = line.Replace(" ", "");

            long partialSum = 0;
            char op = '+';
            for (int i = 0; i < line.Length; i++)
            {
                char c = line[i];
                if (c == '+' || c == '*')
                {
                    op = c;
                }
                else
                {
                    long num = 0;
                    if (c == '(')
                    {
                        int index = FindApproproateClosingBracketPosition(line, i);
                        num = CalcualteSum(line[(i+1)..index]);
                        i = index;
                    }
                    else
                    {
                        num = Convert.ToInt64(c.ToString());
                    }

                    if (op == '+')
                        partialSum += num;
                    else if (op == '*')
                        partialSum *= num;
                }
            }

            return partialSum;
        }

        private int FindApproproateClosingBracketPosition(string line, int index)
        {
            int paranthesesLevel = 0;
            int closingIndex = 0;
            for(int i=index;i<line.Length;i++)
            {
                if (line[i] == '(')
                    paranthesesLevel++;
                else if (line[i] == ')')
                    paranthesesLevel--;

                if (paranthesesLevel == 0)
                {
                    closingIndex = i;
                    break;
                }
            }

            return closingIndex;
        }

        private int FindApproproateOpeningBracketPosition(string line, int index)
        {
            int paranthesesLevel = 0;
            int openingIndex = 0;
            for (int i = index; i >= 0; i--)
            {
                if (line[i] == '(')
                    paranthesesLevel++;
                else if (line[i] == ')')
                    paranthesesLevel--;

                if (paranthesesLevel == 0)
                {
                    openingIndex = i;
                    break;
                }
            }

            return openingIndex;
        }

        protected override string FindSecondSolution(string[] input)
        {
            long sum = 0;
            foreach (string line in input)
            {
                string modifiedLine = line.Replace(" ", "");
                modifiedLine = AddParentheses(modifiedLine);
                sum += CalcualteSum(modifiedLine);
            }
            
            return sum.ToString();
        }

        private string AddParentheses(string line)
        {
            int index = 0;
            while(index <line.Length)
            {
                if(line[index] == '+')
                {
                    int openingIndex = index - 1;
                    if(line[openingIndex] == ')')
                    {
                        openingIndex = FindApproproateOpeningBracketPosition(line, index - 1);
                    }
                    line = line.Insert(openingIndex, "(");

                    int closingIndex = index + 3;
                    if (line[index + 2] == '(')
                    {
                         closingIndex = FindApproproateClosingBracketPosition(line, index+2);
                    }
                    line = line.Insert(closingIndex, ")");
                    index += 1;
                }

                index++;
            }

            return line;
        }
    }
}
