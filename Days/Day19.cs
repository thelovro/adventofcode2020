﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Days
{
    class Day19 : BaseDay
    {
        public Day19()
        {
            SampleInputPartOne.Add(@"0: 1 2
1: ""a""
2: 1 3 | 3 1
3: ""b""

aab
aba
baa
bbb", "2");

            SampleInputPartOne.Add(@"0: 2 4
1: ""a""
2: 1 3 | 3 1
3: ""a""
4: ""b""

aaa
aba
baa
bbb
abb
bab
aab", "1");

            SampleInputPartOne.Add(@"0: 4 1 5
1: 2 3 | 3 2
2: 4 4 | 5 5
3: 4 5 | 5 4
4: ""a""
5: ""b""

ababbb
bababa
abbbab
aaabbb
aaaabbb", "2");

            SampleInputPartTwo.Add(@"42: 9 14 | 10 1
9: 14 27 | 1 26
10: 23 14 | 28 1
1: ""a""
11: 42 31
5: 1 14 | 15 1
19: 14 1 | 14 14
12: 24 14 | 19 1
16: 15 1 | 14 14
31: 14 17 | 1 13
6: 14 14 | 1 14
2: 1 24 | 14 4
0: 8 11
13: 14 3 | 1 12
15: 1 | 14
17: 14 2 | 1 7
23: 25 1 | 22 14
28: 16 1
4: 1 1
20: 14 14 | 1 15
3: 5 14 | 16 1
27: 1 6 | 14 18
14: ""b""
21: 14 1 | 1 14
25: 1 1 | 1 14
22: 14 14
8: 42
26: 14 22 | 1 20
18: 15 15
7: 14 5 | 1 21
24: 14 1

abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa
bbabbbbaabaabba
babbbbaabbbbbabbbbbbaabaaabaaa
aaabbbbbbaaaabaababaabababbabaaabbababababaaa
bbbbbbbaaaabbbbaaabbabaaa
bbbababbbbaaaaaaaabbababaaababaabab
ababaaaaaabaaab
ababaaaaabbbaba
baabbaaaabbaaaababbaababb
abbbbabbbbaaaababbbbbbaaaababb
aaaaabbaabaaaaababaa
aaaabbaaaabbaaa
aaaabbaabbaaaaaaabbbabbbaaabbaabaaa
babaaabbbaaabaababbaabababaaab
aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba", "12");
        }

        protected override string FindFirstSolution(string[] input)
        {
            Dictionary<int, Rule> rules = ParseRules(input);
            List<string> options = ParseOptions(input);

            if (IsTest)
            {
                List<string> validPatterns = ParseValidPatterns(rules);
                int validOptions = options.Intersect(validPatterns).Count();

                return validOptions.ToString();
            }

            rules[0].Pattern = "31";
            List<string> validPatternsThirtyOne = ParseValidPatterns(rules);

            rules[0].Pattern = "42";
            List<string> validPatternsFortyTwo = ParseValidPatterns(rules);

            int targetLength = validPatternsFortyTwo[0].Length * 2 + validPatternsThirtyOne[0].Length;
            int counter = 0;
            foreach (var o in options)
            {
                //it has to start with 42 pattern
                if (!validPatternsFortyTwo.Any(v => o.StartsWith(v)))
                {
                    continue;
                }

                var newOption = o.Remove(0, validPatternsFortyTwo[0].Length);

                //
                if (!validPatternsFortyTwo.Any(v => newOption.StartsWith(v)) || !validPatternsThirtyOne.Any(v => newOption.EndsWith(v)))
                {
                    continue;
                }

                //it has to be of fixed length
                if (o.Length != targetLength)
                    continue;

                counter++;
            }

            return counter.ToString();
        }

        private List<string> ParseValidPatterns(Dictionary<int, Rule> rules)
        {
            Queue<List<string>> openList = new();
            openList.Enqueue(new List<string>() { "0" });

            List<string> patterns = new();
            while (openList.Count > 0)
            {
                var option = openList.Dequeue();

                bool hasOnlyLetters = true;
                List<List<string>> newOptions = new();
                newOptions.Add(new());

                foreach (string item in option)
                {
                    int id;
                    if (int.TryParse(item, out id))
                    {
                        hasOnlyLetters = false;
                        if (rules[id].Pattern.Contains(" | "))
                        {
                            var parts = rules[id].Pattern.Split(" | ");

                            List<List<string>> n = new();
                            foreach (var o in newOptions)
                            {
                                List<string> list1 = new List<string>(o);
                                list1.AddRange(parts[0].Split(" "));
                                List<string> list2 = new List<string>(o);
                                list2.AddRange(parts[1].Split(" "));
                                n.Add(list1);
                                n.Add(list2);
                            }

                            newOptions = n;
                        }
                        else if (rules[id].Pattern.Contains(" "))
                        {
                            foreach (var newOption in newOptions)
                            {
                                newOption.AddRange(rules[id].Pattern.Split(" ").ToList());
                            }
                        }
                        else
                        {
                            foreach (var newOption in newOptions)
                            {
                                newOption.Add(rules[id].Pattern.Replace("\"", ""));
                            }
                        }
                    }
                    else
                    {
                        foreach (var newOption in newOptions)
                        {
                            newOption.Add(item);
                        }
                    }
                }

                if (hasOnlyLetters)
                {
                    patterns.Add(string.Join("", newOptions[0]));
                }
                else
                {
                    foreach (var newOption in newOptions)
                    {
                        openList.Enqueue(newOption);
                    }
                }

            }

            return patterns;
        }

        private List<string> ParseOptions(string[] input)
        {
            int startIndex = Array.IndexOf(input, string.Empty);

            List<string> list = new();
            for (int i = startIndex + 1; i < input.Length; i++)
            {
                list.Add(input[i]);
            }

            return list;
        }

        private Dictionary<int, Rule> ParseRules(string[] input)
        {
            Dictionary<int, Rule> rules = new();
            foreach (var line in input)
            {
                if (line == "") break;

                int id = Convert.ToInt32(line.Split(": ")[0]);
                string part = line.Split(": ")[1];

                string letter = string.Empty;

                rules.Add(id, new Rule() { ID = id, Letter = letter, Pattern = part });
            }

            return rules;
        }

        public class Rule
        {
            public int ID { get; set; }
            public string Letter { get; set; }
            public string Pattern { get; set; }
        }

        protected override string FindSecondSolution(string[] input)
        {
            Dictionary<int, Rule> rules = ParseRules(input);

            //rules[8].Pattern = "42 | 42 8";
            //rules[11].Pattern = "42 31 | 42 11 31";


            //it has to start with at least one 42 and it has to have at least one 42-31 pair!

            List<string> options = ParseOptions(input);

            rules[0].Pattern = "31";
            List<string> validPatternsThirtyOne = ParseValidPatterns(rules);

            rules[0].Pattern = "42";
            List<string> validPatternsFortyTwo = ParseValidPatterns(rules);

            int counter = 0;
            foreach (var o in options)
            {
                var newOption = o;
                int counterBoth = 0;

                //No42 >= No31 + 1
                while (newOption.Length >= (validPatternsThirtyOne[0].Length + validPatternsFortyTwo[0].Length))
                {
                    if (validPatternsFortyTwo.Any(v => newOption.StartsWith(v)) && validPatternsThirtyOne.Any(v => newOption.EndsWith(v)))
                    {
                        newOption = newOption.Remove(0, validPatternsFortyTwo[0].Length);
                        newOption = newOption.Remove(newOption.Length - validPatternsThirtyOne[0].Length);
                        counterBoth++;
                    }
                    else
                    {
                        break;
                    }
                }

                if (counterBoth == 0)
                    continue;

                //there has to be at least one more 42
                int counterFortyTwo = 0;
                while (true)
                {
                    if (validPatternsFortyTwo.Any(v => newOption.StartsWith(v)))
                    {
                        newOption = newOption.Remove(0, validPatternsFortyTwo[0].Length);
                        counterFortyTwo++;
                    }
                    else
                    {
                        break;
                    }
                }

                //very important - at least one more 42 and remaining pattern must be smaller than 8! otherwise it is not OK!
                if (counterFortyTwo > 0 && newOption.Length < validPatternsFortyTwo[0].Length)
                    counter++;
            }

            return counter.ToString();
        }
    }
}
