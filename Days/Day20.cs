﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Days
{
    class Day20 : BaseDay
    {
        public Day20()
        {
            SampleInputPartOne.Add(@"Tile 2311:
..##.#..#.
##..#.....
#...##..#.
####.#...#
##.##.###.
##...#.###
.#.#.#..##
..#....#..
###...#.#.
..###..###

Tile 1951:
#.##...##.
#.####...#
.....#..##
#...######
.##.#....#
.###.#####
###.##.##.
.###....#.
..#.#..#.#
#...##.#..

Tile 1171:
####...##.
#..##.#..#
##.#..#.#.
.###.####.
..###.####
.##....##.
.#...####.
#.##.####.
####..#...
.....##...

Tile 1427:
###.##.#..
.#..#.##..
.#.##.#..#
#.#.#.##.#
....#...##
...##..##.
...#.#####
.#.####.#.
..#..###.#
..##.#..#.

Tile 1489:
##.#.#....
..##...#..
.##..##...
..#...#...
#####...#.
#..#.#.#.#
...#.#.#..
##.#...##.
..##.##.##
###.##.#..

Tile 2473:
#....####.
#..#.##...
#.##..#...
######.#.#
.#...#.#.#
.#########
.###.#..#.
########.#
##...##.#.
..###.#.#.

Tile 2971:
..#.#....#
#...###...
#.#.###...
##.##..#..
.#####..##
.#..####.#
#..#.#..#.
..####.###
..#.#.###.
...#.#.#.#

Tile 2729:
...#.#.#.#
####.#....
..#.#.....
....#..#.#
.##..##.#.
.#.####...
####.#.#..
##.####...
##..#.##..
#.##...##.

Tile 3079:
#.#.#####.
.#..######
..#.......
######....
####.#..#.
.#...#.##.
#.#####.##
..#.###...
..#.......
..#.###...
", "20899048083289");

            SampleInputPartTwo.Add(@"Tile 2311:
..##.#..#.
##..#.....
#...##..#.
####.#...#
##.##.###.
##...#.###
.#.#.#..##
..#....#..
###...#.#.
..###..###

Tile 1951:
#.##...##.
#.####...#
.....#..##
#...######
.##.#....#
.###.#####
###.##.##.
.###....#.
..#.#..#.#
#...##.#..

Tile 1171:
####...##.
#..##.#..#
##.#..#.#.
.###.####.
..###.####
.##....##.
.#...####.
#.##.####.
####..#...
.....##...

Tile 1427:
###.##.#..
.#..#.##..
.#.##.#..#
#.#.#.##.#
....#...##
...##..##.
...#.#####
.#.####.#.
..#..###.#
..##.#..#.

Tile 1489:
##.#.#....
..##...#..
.##..##...
..#...#...
#####...#.
#..#.#.#.#
...#.#.#..
##.#...##.
..##.##.##
###.##.#..

Tile 2473:
#....####.
#..#.##...
#.##..#...
######.#.#
.#...#.#.#
.#########
.###.#..#.
########.#
##...##.#.
..###.#.#.

Tile 2971:
..#.#....#
#...###...
#.#.###...
##.##..#..
.#####..##
.#..####.#
#..#.#..#.
..####.###
..#.#.###.
...#.#.#.#

Tile 2729:
...#.#.#.#
####.#....
..#.#.....
....#..#.#
.##..##.#.
.#.####...
####.#.#..
##.####...
##..#.##..
#.##...##.

Tile 3079:
#.#.#####.
.#..######
..#.......
######....
####.#..#.
.#...#.##.
#.#####.##
..#.###...
..#.......
..#.###...
", "273");
        }

        protected override string FindFirstSolution(string[] input)
        {
            Dictionary<int, Tile> tiles = ParseTiles(input);

            var tileIntersections = GetTileIntersections(tiles);

            long result = 1;
            foreach (var tile in tileIntersections.Where(t=>t.Value.Count==2))
            {
                result *= Convert.ToInt64(tile.Key);
            }

            return result.ToString();
        }

        private Dictionary<int, Tile> ParseTiles(string[] input)
        {
            Dictionary<int, Tile> tiles = new();
            Regex r = new Regex("[0-9]{4}");
            for (int i = 0; i < input.Length; i += 12)
            {
                Tile tile = new()
                {
                    ID = Convert.ToInt32(r.Match(input[i]).Value),
                    Image = new string[10]
                };

                Array.Copy(input, i + 1, tile.Image, 0, 10);

                tiles.Add(tile.ID, tile);
            }

            return tiles;
        }

        protected override string FindSecondSolution(string[] input)
        {
            Dictionary<int, Tile> tiles = ParseTiles(input);
            Dictionary<int, List<string>> tileMatches = GetTileIntersections(tiles);

            string[] image = GetImage(tiles, tileMatches);

            int numberOfMonsters = CountMonsters(image);

            int result = string.Join("", image).Replace(".", "").Length - numberOfMonsters * 15;

            //wait for cca 30s
            return result.ToString();
        }

        private static Dictionary<int, List<string>> GetTileIntersections(Dictionary<int, Tile> tiles)
        {
            Dictionary<int, List<string>> tileMatches = new();

            foreach (var tile in tiles)
            {
                List<string> borderMatches = new();
                foreach (var b in tile.Value.CurrentBorders)
                {
                    if (tiles.Any(t => t.Key != tile.Key && t.Value.AllPossibleBorders.Contains(b)))
                    {
                        borderMatches.Add(b);
                    }
                }

                tileMatches.Add(tile.Key, borderMatches);
            }

            return tileMatches;
        }

        #region Count monsters
        private int CountMonsters(string[] image)
        {
            int counter = 0;
            bool wasLastOperationRotate = true;
            while (counter == 0)
            {
                for (int y = 0; y < image.Length; y++)
                {
                    for (int x = 0; x < image.Length; x++)
                    {
                        counter += CheckForSeaMonster(x, y, image);
                    }
                }
                if (counter > 0) break;

                if (wasLastOperationRotate)
                {
                    image = FlipImageHorizontal(image);
                    wasLastOperationRotate = false;
                }
                else
                {
                    //flip back and rotate
                    image = FlipImageHorizontal(image);
                    image = RotateImageRight(image);
                    wasLastOperationRotate = true;
                }
            }

            return counter;
        }

        private int CheckForSeaMonster(int x, int y, string[] image)
        {
            //                  # 
            //#    ##    ##    ###
            // #  #  #  #  #  #   
            if (x + 19 >= image.Length || y+2 > image.Length)
                return 0;

            List<(int,int)> monsterIds = new()
            {
                (18,0),
                (0, 1), (5, 1), (6, 1), (11, 1), (12, 1), (17, 1), (18, 1), (19, 1),
                (1, 2), (4, 2), (7, 2), (10, 2), (13, 2), (16, 2)
            };

            foreach (var position in monsterIds)
            {
                if (image[y + position.Item2][x + position.Item1] == '.')
                    return 0;
            }

            return 1;
        }

        private string[] RotateImageRight(string[] image)
        {
            string[] array = new string[image.Length];
            for (int x = 0; x < image.Length; x++)
            {
                string row = "";
                for (int y = image.Length - 1; y >= 0; y--)
                {
                    row += image[y][x];
                }
                array[x] = row;
            }

            return array;
        }

        private string[] FlipImageHorizontal(string[] image)
        {
            for (int y = 0; y < image.Length; y++)
            {
                image[y] = new string(image[y].Reverse().ToArray());
            }

            return image;
        }
        #endregion Count monsters

        #region Get image
        private string[] GetImage(Dictionary<int, Tile> tiles, Dictionary<int, List<string>> tileMatches)
        {
            Dictionary<(int, int), int> usedTiles = new();
            PopulateBorderTiles(tiles, tileMatches, usedTiles);
            PopulateInnerTiles(tiles, tileMatches, usedTiles);
            RotateTiles(tiles, tileMatches, usedTiles);
            string[] image = ClipTilesAndGlueThemTogether(tiles, usedTiles);

            return image;
        }

        private static void PopulateBorderTiles(Dictionary<int, Tile> tiles, Dictionary<int, List<string>> tileMatches, Dictionary<(int, int), int> usedTiles)
        {
            //take first tile with 2 matches (corner)
            var first = tileMatches.First(t => t.Value.Count == 2);
            int positionX = 0;
            int positionY = 0;
            usedTiles.Add((positionX++, positionY), first.Key);

            //get border
            Tile borderTile = tiles.First(t => t.Key == first.Key).Value;
            int increaseX = 1;
            int increaseY = 0;
            while (true)
            {
                var borderTileCandidates = tiles
                    .Where(t => !usedTiles.ContainsValue(t.Key))
                    .Where(t => t.Value.AllPossibleBorders.Intersect(borderTile.AllPossibleBorders).Count() > 0)
                    .Where(t => tileMatches[t.Key].Count() < 4)
                    .ToList();

                if (borderTileCandidates.Count() == 0)
                {
                    //border is finished
                    break;
                }

                borderTile = borderTileCandidates.First().Value;

                usedTiles.Add((positionX, positionY), borderTile.ID);

                if (tileMatches[borderTile.ID].Count() == 2)
                {
                    //rotate right
                    if (increaseX == 1 && increaseY == 0) { increaseX = 0; increaseY = 1; }
                    else if (increaseX == 0 && increaseY == 1) { increaseX = -1; increaseY = 0; }
                    else if (increaseX == -1 && increaseY == 0) { increaseX = 0; increaseY = -1; }
                    else if (increaseX == 0 && increaseY == -1) { increaseX = 1; increaseY = 0; }
                }
                positionX += increaseX;
                positionY += increaseY;
            }
        }
        
        private static void PopulateInnerTiles(Dictionary<int, Tile> tiles, Dictionary<int, List<string>> tileMatches, Dictionary<(int, int), int> usedTiles)
        {
            Queue<(int, int)> openItems = new();
            openItems.Enqueue((1, 1));

            while (openItems.Count() > 0)
            {
                var item = openItems.Dequeue();
                var prevTiles = tiles.Where(t => usedTiles
                                                        .Where(u => u.Key.Item1 == item.Item1 - 1 && u.Key.Item2 == item.Item2
                                                                || u.Key.Item1 == item.Item1 && u.Key.Item2 == item.Item2 - 1)
                                                        .Select(c => c.Value)
                                                        .ToList()
                                                        .Contains(t.Key))
                                                .ToList();

                var allPossibleBorders = prevTiles[0].Value.AllPossibleBorders.Union(prevTiles[1].Value.AllPossibleBorders);

                var innerTileCandidates = tiles
                    .Where(t => !usedTiles.ContainsValue(t.Key))
                    .Where(t => t.Value.CurrentBorders.Intersect(allPossibleBorders).Count() == 2);

                //var innerTileCandidatesPrev1 = tiles
                //    .Where(t => !usedTiles.ContainsValue(t.Key))
                //    .Where(t => t.Value.AllPossibleBorders.Intersect(prevTiles[0].Value.AllPossibleBorders).Count() >0);

                //var innerTileCandidatesPrev2 = tiles
                //    .Where(t => !usedTiles.ContainsValue(t.Key))
                //    .Where(t => t.Value.AllPossibleBorders.Intersect(prevTiles[1].Value.AllPossibleBorders).Count() > 0);

                if (innerTileCandidates.Count() == 0)
                {
                    //no more items
                    break;
                }

                var tile = innerTileCandidates.First();

                usedTiles.Add((item.Item1, item.Item2), tile.Key);

                if (!usedTiles.ContainsKey((item.Item1 + 1, item.Item2)) && !openItems.Contains((item.Item1 + 1, item.Item2)))
                    openItems.Enqueue((item.Item1 + 1, item.Item2));
                if (!usedTiles.ContainsKey((item.Item1, item.Item2 + 1)) && !openItems.Contains((item.Item1, item.Item2 + 1)))
                    openItems.Enqueue((item.Item1, item.Item2 + 1));
            }
        }

        private void RotateTiles(Dictionary<int, Tile> tiles, Dictionary<int, List<string>> tileMatches, Dictionary<(int, int), int> usedTiles)
        {
            //rotate and clip first tile
            var currentTile = tiles[usedTiles[(0, 0)]];
            var tileOnRight = tiles[usedTiles[(1, 0)]];
            var tileBelow = tiles[usedTiles[(0, 1)]];

            var rightBorder = currentTile.AllPossibleBorders.Intersect(tileOnRight.AllPossibleBorders);
            var bottomBorder = currentTile.AllPossibleBorders.Intersect(tileBelow.AllPossibleBorders);

            currentTile.RotateFirst(rightBorder, bottomBorder);

            Queue<(int, int, string, string)> openItems = new();
            openItems.Enqueue((1, 0, null, currentTile.RightBorder));
            openItems.Enqueue((0, 1, currentTile.LowerBorder, null));

            while (openItems.Count() > 0)
            {
                var item = openItems.Dequeue();
                currentTile = tiles[usedTiles[(item.Item1, item.Item2)]];

                currentTile.Rotate(item.Item3, item.Item4);

                if (item.Item1 < usedTiles.Max(u => u.Key.Item1))
                {
                    openItems.Enqueue((item.Item1 + 1, item.Item2, null, currentTile.RightBorder));
                }
                if (item.Item2 < usedTiles.Max(u => u.Key.Item2))
                {
                    openItems.Enqueue((item.Item1, item.Item2 + 1, currentTile.LowerBorder, null));
                }
            }
        }

        private string[] ClipTilesAndGlueThemTogether(Dictionary<int, Tile> tiles, Dictionary<(int, int), int> usedTiles)
        {
            int baseSize = (int)Math.Sqrt(tiles.Count);
            string[] finalImage = new string[8 * baseSize];

            for (int y = 0; y < baseSize; y++)
            {
                for (int x = 0; x < baseSize; x++)
                {
                    for (int r = 1; r < 9; r++)
                    {
                        finalImage[y * 8 + r - 1] += tiles[usedTiles[(x, y)]].Image[r][1..9];
                    }
                }
            }

            return finalImage;
        }
        #endregion Get image

        public class Tile
        {
            public int ID { get; set; }
            public string[] Image { get; set; }
            public string UpperBorder { get { return Image[0]; } }
            public string RightBorder { get { return new string(Image.Select(row => row[Image.Length - 1]).ToArray()); } }
            public string LowerBorder { get { return Image[Image.Length - 1]; } }
            public string LeftBorder { get { return new string(Image.Select(row => row[0]).ToArray()); } }

            public List<string> CurrentBorders
            {
                get
                {
                    return new List<string>()
                    {
                        UpperBorder,
                        RightBorder,
                        LowerBorder,
                        LeftBorder,
                    };
                }
            }

            public List<string> AllPossibleBorders
            {
                get
                {
                    return CurrentBorders.Union(CurrentBorders.Select(c => new string(c.Reverse().ToArray())).ToList()).ToList();
                }
            }

            internal void RotateFirst(IEnumerable<string> rightBorder, IEnumerable<string> lowerBorder)
            {
                bool rotatingDone = false;
                if (rightBorder != null)
                {
                    if (!rotatingDone)
                    {
                        rotatingDone = true;
                        while (!rightBorder.Contains(RightBorder))
                        {
                            RotateRight();
                        }
                    }
                    else
                    {
                        FlipHorizontal();
                    }
                }

                if (lowerBorder != null)
                {
                    if (!rotatingDone)
                    {
                        rotatingDone = true;
                        while (!lowerBorder.Contains(LowerBorder))
                        {
                            RotateRight();
                        }
                    }
                    else
                    {
                        FlipVertical();
                    }
                }
            }

            internal void Rotate(string upperBorder, string leftBorder)
            {
                if (upperBorder != null)
                {
                    while (UpperBorder != upperBorder && UpperBorder != new string(upperBorder.Reverse().ToArray()))
                    {
                        RotateRight();
                    }

                    if (UpperBorder != upperBorder)
                    {
                        FlipHorizontal();
                    }
                }

                if (leftBorder != null)
                {
                    while (LeftBorder != leftBorder && LeftBorder != new string(leftBorder.Reverse().ToArray()))
                    {
                        RotateRight();
                    }

                    if (LeftBorder != leftBorder)
                    {
                        FlipVertical();
                    }
                }
            }

            private void RotateRight()
            {
                string[] array = new string[Image.Length];
                for (int x = 0; x < Image.Length; x++)
                {
                    string row = "";
                    for (int y = Image.Length - 1; y >= 0; y--)
                    {
                        row += Image[y][x];
                    }
                    array[x] = row;
                }

                Image = array;
            }

            private void FlipHorizontal()
            {
                for (int y = 0; y < Image.Length; y++)
                {
                    Image[y] = new string(Image[y].Reverse().ToArray());
                }
            }

            private void FlipVertical()
            {
                string[] array = new string[Image.Length];
                for (int y = 0; y < Image.Length; y++)
                {
                    array[Image.Length - 1 - y] = new string(Image[y]);
                }

                Image = array;
            }
        }
    }
}
