﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Days
{
    class Day21 : BaseDay
    {
        public Day21()
        {
            SampleInputPartOne.Add(@"mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
trh fvjkl sbzzf mxmxvkd (contains dairy)
sqjhc fvjkl (contains soy)
sqjhc mxmxvkd sbzzf (contains fish)", "5");

            SampleInputPartTwo.Add(@"mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
trh fvjkl sbzzf mxmxvkd (contains dairy)
sqjhc fvjkl (contains soy)
sqjhc mxmxvkd sbzzf (contains fish)", "mxmxvkd,sqjhc,fvjkl");
        }

        protected override string FindFirstSolution(string[] input)
        {
            List<(List<string>, List<string>)> foods = ParseFoods(input);
            List<string> ingredients = ParseIngredients(foods);

            //find ingredients with alergens
            Dictionary<string, List<string>> alergenIngredients = ParseAlergensIngredients(foods);

            //remove ingredients with alergens from list
            foreach (var ai in alergenIngredients)
            {
                foreach (var a in ai.Value)
                {
                    if (ingredients.Contains(a))
                    {
                        ingredients.Remove(a);
                    }
                }
            }

            //count how many times remaining ingredients exist in foods
            int counter = foods.Sum(f => ingredients.Where(i => f.Item1.Contains(i)).Count());

            return counter.ToString();
        }

        private List<string> ParseIngredients(List<(List<string>, List<string>)> foods)
        {
            List<string> ingredients = foods.SelectMany(f => f.Item1).Distinct().ToList();

            return ingredients;
        }

        private Dictionary<string, List<string>> ParseAlergensIngredients(List<(List<string>, List<string>)> foods)
        {
            Dictionary<string, List<string>> alergensIngredients = new();
            foreach (var food in foods)
            {
                foreach (var alergen in food.Item2)
                {
                    if (!alergensIngredients.ContainsKey(alergen))
                    {
                        alergensIngredients.Add(alergen, food.Item1);
                    }
                    else
                    {
                        alergensIngredients[alergen] = alergensIngredients[alergen].Intersect(food.Item1).ToList();
                    }
                }
            }

            return alergensIngredients;
        }

        private List<(List<string>, List<string>)> ParseFoods(string[] input)
        {
            List<(List<string>, List<string>)> foods = new();
            foreach (string line in input)
            {
                var parts = line.Replace(")", "").Split(" (contains ");

                foods.Add((parts[0].Split(" ").ToList(), parts[1].Split(", ").ToList()));
            }

            return foods;
        }

        protected override string FindSecondSolution(string[] input)
        {
            List<(List<string>, List<string>)> foods = ParseFoods(input);
            Dictionary<string, List<string>> alergenIngredients = ParseAlergensIngredients(foods);

            List<(string, string)> alergensInIngredient = ParseExactAlergensForIngredient(alergenIngredients);

            //sort by ingredient
            alergensInIngredient.Sort();

            //make a list of ingredients
            List<string> ingredients = alergensInIngredient.Select(ai => ai.Item2).ToList();

            return string.Join(",", ingredients).ToString();
        }

        private List<(string, string)> ParseExactAlergensForIngredient(Dictionary<string, List<string>> alergenIngredients)
        {
            List<(string, string)> items = new();
            while (true)
            {
                if (!alergenIngredients.Any(a => a.Value.Count == 1))
                    break;

                //there is always one alergen with only one ingredient
                var ingredientCandidate = alergenIngredients.First(a => a.Value.Count == 1);

                var ingredient = ingredientCandidate.Value.First();
                items.Add((ingredientCandidate.Key, ingredient));

                //remove ingredient from all other lists
                foreach (var a in alergenIngredients.Where(a => a.Value.Contains(ingredient)))
                {
                    a.Value.Remove(ingredient);
                }
            }

            return items;
        }
    }
}
