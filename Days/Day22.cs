﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Days
{
    class Day22 : BaseDay
    {
        public Day22()
        {
            SampleInputPartOne.Add(@"Player 1:
9
2
6
3
1

Player 2:
5
8
4
7
10
", "306");

            SampleInputPartTwo.Add(@"Player 1:
43
19

Player 2:
2
29
14", "105");

            SampleInputPartTwo.Add(@"Player 1:
9
2
6
3
1

Player 2:
5
8
4
7
10
", "291");
        }

        protected override string FindFirstSolution(string[] input)
        {
            List<int> cards = ParseCards(input);
            cards = PlayTheGameCombat(cards);

            int result = 0;
            for (int i = 0; i <cards.Count ; i++)
            {
                result += (cards.Count - i) * cards[i];
            }
            return result.ToString();
        }

        private List<int> PlayTheGameCombat(List<int> cards)
        {
            Queue<int> playerOne = new();
            Queue<int> playerTwo = new();

            for (int i = 0; i < cards.Count; i++)
            {
                if(i<cards.Count/2)
                    playerOne.Enqueue(cards[i]);
                else
                    playerTwo.Enqueue(cards[i]);
            }

            while(playerOne.Count >0 && playerTwo.Count > 0)
            {
                int pOneCard = playerOne.Dequeue();
                int pTwoCard = playerTwo.Dequeue();

                if(pOneCard >pTwoCard)
                {
                    playerOne.Enqueue(pOneCard);
                    playerOne.Enqueue(pTwoCard);
                }
                else
                {
                    playerTwo.Enqueue(pTwoCard);
                    playerTwo.Enqueue(pOneCard);
                }
            }

            cards = new();
            while (playerOne.Count > 0 )
            {
                cards.Add(playerOne.Dequeue());
            }
            while (playerTwo.Count > 0)
            {
                cards.Add(playerTwo.Dequeue());
            }

            return cards;
        }

        private List<int> ParseCards(string[] input)
        {
            List<int> playerCards = new();
            foreach (string line in input.Skip(1))
            {
                if (line.StartsWith("Player") || line == "") continue;
                playerCards.Add(Convert.ToInt32(line));
            }

            return playerCards;
        }

        protected override string FindSecondSolution(string[] input)
        {
            List<int> cards = ParseCards(input);

            cards = PlayTheGameRecursiveCombatBase(cards);

            int result = 0;
            for (int i = 0; i < cards.Count; i++)
            {
                result += (cards.Count - i) * cards[i];
            }
            return result.ToString();
        }


        private List<int> PlayTheGameRecursiveCombatBase(List<int> cards)
        {
            Queue<int> playerOne = new();
            Queue<int> playerTwo = new();

            for (int i = 0; i < cards.Count; i++)
            {
                if (i < cards.Count / 2)
                    playerOne.Enqueue(cards[i]);
                else
                    playerTwo.Enqueue(cards[i]);
            }

            (bool didPlayerOneWin, List<int> winnersCards) = PlayTheGameRecursiveCombat(playerOne, playerTwo);

            return winnersCards;
        }

        private (bool, List<int>) PlayTheGameRecursiveCombat(Queue<int> playerOne, Queue<int> playerTwo)
        {
            List<string> playerOneDeckSequences = new();
            List<string> playerTwoDeckSequences = new();

            bool isInnfiniteGameWin = false;
            while (playerOne.Count > 0 && playerTwo.Count > 0)
            {
                //check for infinite game
                string playerOneDeckSequence = string.Join("", playerOne.ToArray());
                string playerTwoDeckSequence = string.Join("", playerTwo.ToArray());
                if (playerOneDeckSequences.Contains(playerOneDeckSequence) 
                    && playerTwoDeckSequences.Contains(playerTwoDeckSequence))
                {
                    isInnfiniteGameWin = true;
                    break;
                }

                playerOneDeckSequences.Add(playerOneDeckSequence);
                playerTwoDeckSequences.Add(playerTwoDeckSequence);

                int pOneCard = playerOne.Dequeue();
                int pTwoCard = playerTwo.Dequeue();

                if(pOneCard <= playerOne.Count && pTwoCard <= playerTwo.Count)
                {
                    //recursive Combat
                    Queue<int> playerOneRecursive = new();
                    Queue<int> playerTwoRecursive = new();

                    foreach(var card in playerOne.ToList().Take(pOneCard))
                        playerOneRecursive.Enqueue(card);
                    foreach (var card in playerTwo.ToList().Take(pTwoCard))
                        playerTwoRecursive.Enqueue(card);

                    (bool didPlayerOneWinRecursive, List<int> winnersCards) = PlayTheGameRecursiveCombat(playerOneRecursive, playerTwoRecursive);
                    if(didPlayerOneWinRecursive)
                    {
                        playerOne.Enqueue(pOneCard);
                        playerOne.Enqueue(pTwoCard);
                    }
                    else
                    {
                        playerTwo.Enqueue(pTwoCard);
                        playerTwo.Enqueue(pOneCard);
                    }
                }
                else
                {
                    if (pOneCard > pTwoCard)
                    {
                        playerOne.Enqueue(pOneCard);
                        playerOne.Enqueue(pTwoCard);
                    }
                    else
                    {
                        playerTwo.Enqueue(pTwoCard);
                        playerTwo.Enqueue(pOneCard);
                    }
                }
            }

            bool didPlayerOneWin = playerTwo.Count == 0;

            List<int> cards = new();
            if (isInnfiniteGameWin)
            {
                didPlayerOneWin = true;
                while (playerOne.Count > 0)
                {
                    cards.Add(playerOne.Dequeue());
                }
            }
            else
            {
                while (playerOne.Count > 0)
                {
                    cards.Add(playerOne.Dequeue());
                }
                while (playerTwo.Count > 0)
                {
                    cards.Add(playerTwo.Dequeue());
                }
            }

            return (didPlayerOneWin, cards);
        }
    }
}
