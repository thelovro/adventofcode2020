﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Days
{
    class Day23 : BaseDay
    {
        public Day23()
        {
            SampleInputPartOne.Add(@"389125467", "67384529");

            SampleInputPartTwo.Add(@"389125467", "149245887792");
        }

        protected override string FindFirstSolution(string[] input)
        {
            Dictionary<int, int> items = ParseItems(input[0], input[0].Length);
            PlayTheGame(items, 100);

            string result = GetResult(items);

            return result.ToString();
        }

        private void PlayTheGame(Dictionary<int, int> items, int cycles)
        {
            var currentItemId = items.First().Key;
            for (int i = 0; i < cycles; i++)
            {
                List<int> skipItems = new();
                skipItems.Add(items[currentItemId]);
                skipItems.Add(items[items[currentItemId]]);
                skipItems.Add(items[items[items[currentItemId]]]);

                int nextItemId = items[items[items[items[currentItemId]]]];

                int destionationItemId = currentItemId - 1;
                while (true)
                {
                    if (destionationItemId == 0)
                        destionationItemId = items.Max(i => i.Key);

                    if (!skipItems.Contains(destionationItemId))
                        break;
                    destionationItemId--;
                }

                items[currentItemId] = nextItemId;
                items[skipItems[2]] = items[destionationItemId];
                items[destionationItemId] = skipItems[0];

                currentItemId = items[currentItemId];
            }
        }

        private string GetResult(Dictionary<int, int> items)
        {
            var firstItem = items.First(i => i.Key == 1);
            var currentItem = items.First(i => i.Key == firstItem.Value);

            string solution = "";
            while (firstItem.Key != currentItem.Key)
            {
                solution += currentItem.Key.ToString();
                currentItem = items.First(i => i.Key == currentItem.Value);
            }

            return solution;
        }

        private Dictionary<int, int> ParseItems(string input, int maxLength)
        {
            Dictionary<int, int> items = new();
            for (int i = 0; i < maxLength; i++)
            {
                int currentNumber = i < input.Length ? Convert.ToInt32(input[i].ToString()) : i+1;
                int nextNumber = i+1 < input.Length ? Convert.ToInt32(input[i + 1].ToString()) : i+1 == maxLength ? Convert.ToInt32(input[0].ToString()) : i+2;
                items.Add(currentNumber, nextNumber);
            }
            return items;
        }

        protected override string FindSecondSolution(string[] input)
        {
            Dictionary<int, int> items = ParseItems(input[0], 1000000);
            PlayTheGame(items, 10000000);

            long numberOne = Convert.ToInt64(items[1]);
            long numberTwo = Convert.ToInt64(items[items[1]]);

            return (numberOne * numberTwo).ToString();
        }
    }
}
