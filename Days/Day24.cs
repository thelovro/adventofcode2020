﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Days
{
    class Day24 : BaseDay
    {
        public Day24()
        {
            SampleInputPartOne.Add(@"sesenwnenenewseeswwswswwnenewsewsw
neeenesenwnwwswnenewnwwsewnenwseswesw
seswneswswsenwwnwse
nwnwneseeswswnenewneswwnewseswneseene
swweswneswnenwsewnwneneseenw
eesenwseswswnenwswnwnwsewwnwsene
sewnenenenesenwsewnenwwwse
wenwwweseeeweswwwnwwe
wsweesenenewnwwnwsenewsenwwsesesenwne
neeswseenwwswnwswswnw
nenwswwsewswnenenewsenwsenwnesesenew
enewnwewneswsewnwswenweswnenwsenwsw
sweneswneswneneenwnewenewwneswswnese
swwesenesewenwneswnwwneseswwne
enesenwswwswneneswsenwnewswseenwsese
wnwnesenesenenwwnenwsewesewsesesew
nenewswnwewswnenesenwnesewesw
eneswnwswnwsenenwnwnwwseeswneewsenese
neswnwewnwnwseenwseesewsenwsweewe
wseweeenwnesenwwwswnew", "10");

            SampleInputPartTwo.Add(@"sesenwnenenewseeswwswswwnenewsewsw
neeenesenwnwwswnenewnwwsewnenwseswesw
seswneswswsenwwnwse
nwnwneseeswswnenewneswwnewseswneseene
swweswneswnenwsewnwneneseenw
eesenwseswswnenwswnwnwsewwnwsene
sewnenenenesenwsewnenwwwse
wenwwweseeeweswwwnwwe
wsweesenenewnwwnwsenewsenwwsesesenwne
neeswseenwwswnwswswnw
nenwswwsewswnenenewsenwsenwnesesenew
enewnwewneswsewnwswenweswnenwsenwsw
sweneswneswneneenwnewenewwneswswnese
swwesenesewenwneswnwwneseswwne
enesenwswwswneneswsenwnewswseenwsese
wnwnesenesenenwwnenwsewesewsesesew
nenewswnwewswnenesenwnesewesw
eneswnwswnwsenenwnwnwwseeswneewsenese
neswnwewnwnwseenwseesewsenwsweewe
wseweeenwnesenwwwswnew", "2208");
        }

        protected override string FindFirstSolution(string[] input)
        {
            Dictionary<(int, int), bool> tiles = PlaceTiles(input);

            var solution = tiles.Where(b => b.Value).Count();

            return solution.ToString();
        }

        private Dictionary<(int, int), bool> PlaceTiles(string[] input)
        {
            Dictionary<(int, int), bool> tiles = new();

            foreach (var line in input)
            {
                var position = (0, 0);
                List<string> directions = ParseDirections(line);
                foreach (var d in directions)
                {
                    position = d switch
                    {
                        "e" => (position.Item1 + 2, position.Item2),
                        "w" => (position.Item1 - 2, position.Item2),
                        "ne" => (position.Item1 + 1, position.Item2 + 1),
                        "nw" => (position.Item1 - 1, position.Item2 + 1),
                        "se" => (position.Item1 + 1, position.Item2 - 1),
                        "sw" => (position.Item1 - 1, position.Item2 - 1),
                        _ => throw new NotImplementedException()
                    };
                }

                if (tiles.ContainsKey(position))
                {
                    tiles[position] = !tiles[position];
                }
                else
                {
                    tiles.Add(position, true);
                }
            }

            return tiles;
        }

        private List<string> ParseDirections(string line)
        {
            List<string> directions = new();
            int i= 0;
            while(i <line.Length)
            {
                if(line[i] == 'e' || line[i] == 'w')
                {
                    directions.Add(line[i].ToString());
                    i++;
                }
                else
                {
                    directions.Add(line.Substring(i, 2));
                    i += 2;
                }
            }

            return directions;
        }

        protected override string FindSecondSolution(string[] input)
        {
            Dictionary<(int, int), bool> tiles = PlaceTiles(input);

            BeCreative(tiles);

            var solution = tiles.Where(b => b.Value).Count();

            return solution.ToString();
        }

        private void BeCreative(Dictionary<(int, int), bool> tiles)
        {
            for (int i = 0; i < 100; i++)
            {
                //populate tiles around existing ones - so no black tile is touching the corner!
                AddAdditionalTiles(tiles);

                List<(int,int)> turnPositions = FlipIfNeeded(tiles);
                foreach(var p in turnPositions)
                {
                    tiles[p] = !tiles[p];
                }
            }
        }

        private void AddAdditionalTiles(Dictionary<(int, int), bool> tiles)
        {
            List<(int, int)> candidates = new();
            foreach (var tile in tiles)
            {
                //we add tiles only around black ones
                if (!tile.Value)
                {
                    continue;
                }

                //check all 6 directions
                foreach (var a in GetAdjacentTiles(tile.Key))
                {
                    if (candidates.Contains(a))
                    {
                        continue;
                    }

                    candidates.Add(a);
                }
            }

            foreach (var c in candidates)
            {
                if (tiles.ContainsKey(c))
                {
                    continue;
                }

                tiles.Add(c, false);
            }
        }

        private List<(int, int)> FlipIfNeeded(Dictionary<(int, int), bool> tiles)
        {
            List<(int, int)> candidates = new();
            foreach(var tile in tiles)
            {
                int countBlack = 0;
                foreach(var a in GetAdjacentTiles(tile.Key))
                {
                    if (tiles.ContainsKey(a) && tiles[a]) countBlack++;
                }

                //is flip needed?
                if((tile.Value && (countBlack == 0 || countBlack>2))
                    || (!tile.Value && countBlack == 2))
                {
                    candidates.Add(tile.Key);
                }    
            }

            return candidates;
        }

        private List<(int,int)> GetAdjacentTiles((int,int) tile)
        {
            return new List<(int, int)>()
            {
                (tile.Item1 - 2, tile.Item2),
                (tile.Item1 - 1, tile.Item2 + 1),
                (tile.Item1 + 1, tile.Item2 + 1),
                (tile.Item1 + 2, tile.Item2),
                (tile.Item1 + 1, tile.Item2 - 1),
                (tile.Item1 - 1, tile.Item2 - 1)
            };
        }
    }
}
