﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Days
{
    class Day25 : BaseDay
    {
        public Day25()
        {
            SampleInputPartOne.Add(@"5764801
17807724", "14897079");
        }

        protected override string FindFirstSolution(string[] input)
        {
            long cardPublicKey = Convert.ToInt64(input[0]);
            long doorPublicKey = Convert.ToInt64(input[1]);

            long cardLoopSize = DetermineLoopSize(cardPublicKey);
            long doorLoopSize = DetermineLoopSize(doorPublicKey);

            long result = MakeHandshake(doorPublicKey, cardLoopSize);

            return result.ToString();
        }

        private long DetermineLoopSize(long publicKey)
        {
            long value = 1;
            long subjectNumber = 7;
            long dividor = 20201227;

            long counter = 0;
            while(value != publicKey)
            {
                value *= subjectNumber;
                value = value % dividor;

                counter++;
            }

            return counter;
        }

        private long MakeHandshake(long publicKey, long loopSize)
        {
            long value = 1;
            long subjectNumber = publicKey;
            long dividor = 20201227;

            long counter = 0;
            while(counter < loopSize)
            {
                value *= subjectNumber;
                value = value % dividor;

                counter++;
            }

            return value;
        }

        protected override string FindSecondSolution(string[] input)
        {
            return "";
        }
    }
}
